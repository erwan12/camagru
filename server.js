const Koa = require('koa')
require('dotenv').config({ path: './config/.env' })
const Router = require('@koa/router')
const bodyParser = require('koa-bodyparser')
const app = new Koa()
const router = new Router()
const PORT = process.env.PORT || 3000

app.use(bodyParser())

app.use((ctx, next) => {
  ctx.body = `Server start on ${PORT}`
  next()
})

app.use(router.routes()).use(router.allowedMethods())

app.listen(PORT, () => {
  console.log(`Server start on ${PORT}`)
})
